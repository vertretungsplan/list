/*
 * vertretungsplan.io list
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const express = require('express')
const data = require('../data/output.json')

const app = express()

app.get('/vp-content', (req, res) => {
  const oldHost = 'localhost'
  let newHost = req.headers['host'] || oldHost

  if (newHost.indexOf(':') !== -1) {
    newHost = newHost.substr(0, newHost.indexOf(':'))
  }

  res.json({
    institutions: data.institutions.map((item) => ({
      ...item,
      contentServerUrl: item.contentServerUrl.replace(oldHost, newHost)
    }))
  })
})

app.listen(process.env.PORT || 8080)

console.log('ready')
