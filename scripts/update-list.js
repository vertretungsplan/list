/*
 * vertretungsplan.io list
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const { readFileSync, writeFileSync } = require('fs')
const { resolve } = require('path')
const serverContentPath = resolve(__dirname, '../data/servercontent.json')
const institutionsInternalPath = resolve(__dirname, '../data/institutions.json')
const outputPath = resolve(__dirname, '../data/output.json')

const serverContent = JSON.parse(readFileSync(serverContentPath))

// get status
const currentSourcesByInstitutionId = {}

{
  let currentInternalInstitutions

  try {
    currentInternalInstitutions = JSON.parse(readFileSync(institutionsInternalPath))
  } catch (ex) {
    currentInternalInstitutions = {}
  }

  const institutionIds = Object.keys(currentInternalInstitutions)

  for (let i = 0; i < institutionIds.length; i++) {
    const institutionId = institutionIds[i]

    currentSourcesByInstitutionId[institutionId] = currentInternalInstitutions[institutionId].selectedSource
  }
}

// update institution server list
const institutionsInternal = {}

const servers = Object.keys(serverContent)
for (let i = 0; i < servers.length; i++) {
  const server = servers[i]
  const institutions = serverContent[server]

  for (let j = 0; j < institutions.length; j++) {
    const institution = institutions[j]

    if (!institutionsInternal[institution.id]) {
      institutionsInternal[institution.id] = {
        sources: [],
        selectedSource: currentSourcesByInstitutionId[institution.id] || ''
      }
    }

    institutionsInternal[institution.id].sources.push(institution.url)
  }
}

// do validation
{
  const institutions = Object.keys(institutionsInternal)

  for (let i = 0; i < institutions.length; i++) {
    const institutionId = institutions[i]
    const institution = institutionsInternal[institutionId]

    if (institution.selectedSource) {
      if (institution.sources.indexOf(institution.selectedSource) === -1) {
        institution.mode = 'unknown source'
      } else {
        const matchingServerContent = serverContent[institution.selectedSource]

        if (!matchingServerContent) {
          institution.mode = 'server not found'
          continue
        }

        const matchingEntry = matchingServerContent.find((item) => item.id === institutionId)

        if (!matchingEntry) {
          institution.mode = 'institution not found at server'
          continue
        }

        institution.mode = 'OK'
        institution.title = matchingEntry.title
      }
    } else {
      institution.mode = 'no source selected'
    }
  }
}

// write status
writeFileSync(institutionsInternalPath, JSON.stringify(institutionsInternal, null, 2))

// write output file
{
  const output = {
    institutions: Object.keys(institutionsInternal).map((institutionId) => ({
      id: institutionId,
      data: institutionsInternal[institutionId]
    })).filter((item) => item.data.mode === 'OK')
      .map((item) => ({
        id: item.id,
        title: item.data.title,
        contentServerUrl: item.data.selectedSource
      }))
  }

  writeFileSync(outputPath, JSON.stringify(output, null, 2))
}
