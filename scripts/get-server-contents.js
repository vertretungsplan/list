/*
 * vertretungsplan.io list
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const { readFileSync, writeFileSync } = require('fs')
const { resolve } = require('path')
const request = require('request-promise-native')
const servers = require('./servers.js')

const serverContentPath = resolve(__dirname, '../data/servercontent.json')

function getCurrentServerContents() {
  try {
    return JSON.parse(readFileSync(serverContentPath))
  } catch (ex) {
    console.warn('could not get cached content file, creating new one')
    return {}
  }
}

async function main() {
  const serverContents = getCurrentServerContents()

  {
    // delete eventually old entries
    const keys = Object.keys(serverContents)

    for (let i = 0; i < keys.length; i++) {
      const key = keys[i]

      if (servers.indexOf(key) === -1) {
        delete serverContents[key]
        console.log('remove server ' + key + ' from cache because it was removed from the source list')
      }
    }
  }

  {
    // query all servers
    await Promise.all(servers.map(async (server) => {
      try {
        const response = await request({
          url: server + '/vp-content',
          json: true
        })

        if (typeof response !== 'object') {
          throw new Error('bad response')
        }

        const institutions = response.institutions

        if (!Array.isArray(institutions)) {
          throw new Error('bad response')
        }

        institutions.forEach((item) => {
          item.url = item.url || server

          if (typeof item !== 'object' || typeof item.id !== 'string' || typeof item.title !== 'string' || typeof item.url !== 'string' || Object.keys(item).length !== 3) {
            throw new Error('bad response')
          }

          if (item.url !== server) {
            throw new Error('including listing servers is not supported')
          }
        })

        serverContents[server] = institutions
      } catch (ex) {
        console.warn('could not update ' + server, ex)
      }
    }))
  }

  {
    // save new file
    writeFileSync(serverContentPath, JSON.stringify(serverContents, null, 2))

    console.log('new server content list file written')
  }
}

main().catch((ex) => {
  console.warn(ex)
  process.exit(1)
})
