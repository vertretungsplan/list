# Vertretungsplan.io list

This repository contains a tool to get the institutions from other servers,
filter this list and create a institution list for the client application.

## Files (in the data directory)

### servers.txt

- list of servers which should be used
- one line per url

### servercontent.json

- content of the servers from the servers of servers.txt
- updated by ``npm run get-server-contents``

### institutions.json

- lists the institutions, the possible source and the selected source
- is updated by ``npm run update-list``
- can be modified manually to select the server

### output.json

- list of the institutions for the client/ list server
- is updated by ``npm run update-list``

## Commands

### npm run get-server-contents

- reads the server list from ``servers.txt``
- updates ``servercontent.json``

### npm run update-list

- reads ``servercontent.json`` and ``institutions.json``
- updates ``institutions.json`` and output.json

### npm run serve-list

- serves ``output.json`` on port 8080 so that the App can use it as a list server

## License

AGPL 3.0

> vertretungsplan.io list
> Copyright (C) 2019 Jonas Lochmann
>
> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as
> published by the Free Software Foundation, version 3 of the
> License.
>
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program.  If not, see <https://www.gnu.org/licenses/>.
